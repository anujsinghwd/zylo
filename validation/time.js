const isEmpty = require('./is_empty');
const Validator = require("fastest-validator");
const v = new Validator();

module.exports = function validateInput(data){
    let errors = {};
    
    data.y1 = !isEmpty(data.y1) ? data.y1 : '';
    data.y2 = !isEmpty(data.y2) ? data.y2 : '';
    data.y3 = !isEmpty(data.y3) ? data.y3 : '';
    data.timestamp = !isEmpty(data.timestamp) ? data.timestamp : '';
    
    const schema = {
        y1: { type: "number", positive: false, integer: true },
        y2: { type: "number", positive: false, integer: true },
        y3: { type: "number", positive: false, integer: true },
        timestamp: { type: "number", positive: true, integer: true },
    };

    let err = v.validate({ y1: data.y1, y2: data.y2, y3: data.y3, timestamp: data.timestamp }, schema);
    if(err !== true){
        err.forEach(element => {
            (element.field === 'y1') ? (errors.y1 = element.message) : '';
            (element.field === 'y2') ? (errors.y2 = element.message) : '';
            (element.field === 'y3') ? (errors.y3 = element.message) : '';
            (element.field === 'timestamp') ? (errors.timestamp = element.message) : '';
        });
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    }
};