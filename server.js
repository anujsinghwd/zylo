const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
app.use(cors());

const times = require('./routes/api/times');

//Body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// DB Config 
const db = require('./config/keys').MONGO_DB_URL;

// Connect to MongoDB
mongoose
    .connect(db, {useNewUrlParser: true})
    .then(() => console.log('MongoDB connected'))
    .catch(er => console.log(err));

// Use Routes
app.use('/api', times);

const PORT =  process.env.PORT || 5002;

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});