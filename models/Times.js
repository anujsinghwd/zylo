const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const TimeSchema = new Schema({
    plot_data: [
        {
            y1: {
                type: Number,
                require: true
            },
            y2: {
               type: Number,
               require: true
           },
           y3: {
               type: Number,
               require: true
           },
           timestamp: {
               type: Number,
               require: true
           }
        }
    ],   
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = Time = mongoose.model('times', TimeSchema);