const express = require('express');
const router = express.Router();
const moment = require('moment');

// Load Input Validation
const validateInput = require('../../validation/time');

// Load times Model
const Time = require('../../models/Times');

function getTimeLimit(){
    let d = new Date();
    let milliseconds = Date.parse(d);
    milliseconds = milliseconds - (5 * 60 * 1000);
    return milliseconds;
}

router.post('/save', (req, res) => {
    const limitedTime = getTimeLimit();
    const {errors, isValid} = validateInput(req.body);

    // Cheack Validation
    if(!isValid){
        return res.status(400).json({error: errors});
    }

    const plot_data = {
        y1: req.body.y1,
        y2: req.body.y2,
        y3: req.body.y3,
        timestamp: req.body.timestamp
    };

    Time
        .find()
        .then((plot) => {
            if(plot.length > 0){
                plot[0].plot_data.push(plot_data);
                let filterData = plot[0].plot_data.filter(e => e.timestamp >= limitedTime);
                plot[0].save().then(result => res.json({data: filterData}));
            }
            else{
                const newTime = new Time({
                    plot_data: [plot_data]
                });

                newTime
                    .save()
                    .then(time => res.json({data: time}))
                    .catch(err => console.log(err));
            }
        })
        .catch((err) => console.log(err));
});

router.get('/fetch', (req, res) => {
    const limitedTime = getTimeLimit();
    Time
        .find()
        .sort({'plot_data.timestamp': 'desc'})
        .then((plot) => {
            if(plot){
                // plot[0]
                // let data = [];
                // plot[0].plot_data.forEach(element => {
                //     // console.log(element);
                //     if(element.timestamp >= limitedTime){
                //         data.push({nodatafound})
                //     }
                // })
                let filterData = plot[0].plot_data.filter(e => e.timestamp >= limitedTime);
                return res.json({data: filterData});
            }
            res.json({nodatafound: 'Oops!'});
        })
        .catch((err) => console.log(err));
})

module.exports = router;