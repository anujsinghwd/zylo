## API EndPoints

### Save Data
- Type - POST
- `http://51.15.196.66:5002/api/save`

#### params
- y1
- y2
- y3
- timestamp

### Fetch Data
- Type - GET
- `http://51.15.196.66:5002/api/fetch`

